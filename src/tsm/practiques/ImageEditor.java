/*
 * ImageEditor.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsm.practiques;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author mat.aules
 */
public class ImageEditor {

    List<pTSM> originalImages;

    // BufferedImage bi;
    // WritableRaster wr;

    public ImageEditor(List<pTSM> images) {
        originalImages = images;
    }

    private double[] getGreyValue(int[] pixel) {
        double temp = 0.0;
        double[] ret = new double[pixel.length];
        for (int i = 0; i < pixel.length; i++) {
            temp += pixel[i];
        }
        temp = temp / pixel.length;

        for (int j = 0; j < pixel.length; j++) {
            ret[j] = temp;
        }
        return ret;
    }

    private double[] getInverseValue(int[] pixel) {
        double[] ret = new double[pixel.length];
        for (int i = 0; i < pixel.length; i++) {
            ret[i] = 255.0 - pixel[i];
        }
        return ret;
    }

    public double[] getBrightValue(int[] pixel, int bright) {
        double temp2 = 0.0;
        double[] ret = new double[pixel.length];
        for (int i = 0; i < pixel.length; i++) {
            temp2 = (pixel[i] * (bright + 25.0)) / 25.0;
            if (temp2 < 255) {
                ret[i] = temp2;
            } else {
                ret[i] = 255.0;
            }

        }
        return ret;
    }

    public double[] getThresholdValue(int[] pixel, int threshold) {

        double temp = 0.0;
        double[] ret = new double[pixel.length];
        for (int i = 0; i < pixel.length; i++) {
            temp += pixel[i];
        }
        temp = temp / pixel.length;

        for (int j = 0; j < pixel.length; j++) {
            if (temp <= threshold) {
                ret[j] = 0;
            } else {
                ret[j] = 255;
            }
        }
        return ret;
    }

    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public ArrayList<pTSM> inverseEffect() {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2 = getInverseValue(pix);

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }

        return imagesModified;
    }

    public ArrayList<pTSM> grayScaleEffect() {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2 = getGreyValue(pix);

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }

        return imagesModified;
    }

    public ArrayList<pTSM> rChannelEffect() {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            pix2 = new double[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2[0] = pix[0];
                    pix2[1] = 0.0;
                    pix2[2] = 0.0;

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }
        return imagesModified;
    }

    public ArrayList<pTSM> gChannelEffect() {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            pix2 = new double[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2[0] = 0.0;
                    pix2[1] = pix[1];
                    pix2[2] = 0.0;

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }
        return imagesModified;
    }

    public ArrayList<pTSM> bChannelEffect() {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            pix2 = new double[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2[0] = 0.0;
                    pix2[1] = 0.0;
                    pix2[2] = pix[2];

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }
        return imagesModified;
    }

    public ArrayList<pTSM> brightnessEffect(int brigth) {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2 = getBrightValue(pix, brigth);

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }

        return imagesModified;
    }

    public ArrayList<pTSM> thresholdEffect(int threshold) {
        ArrayList<pTSM> imagesModified = new ArrayList<pTSM>();
        BufferedImage bi;
        BufferedImage bi2;
        WritableRaster wr;
        int[] pix;
        double[] pix2;

        for (int images = 0; images < originalImages.size(); images++) {
            bi = originalImages.get(images).getBufImg();
            wr = ((WritableRaster) bi.getData());

            pix = new int[wr.getNumBands()];
            for (int i = 0; i < bi.getWidth(); i++) {
                for (int j = 0; j < bi.getHeight(); j++) {
                    pix = wr.getPixel(i, j, pix);
                    pix2 = getThresholdValue(pix, threshold);

                    wr.setPixel(i, j, pix2);
                }
            }

            bi2 = deepCopy(bi);
            bi2.setData(wr);

            imagesModified.add(new pTSM(bi2, originalImages.get(images)
                    .getNomImatge()));
        }

        return imagesModified;
    }

}
