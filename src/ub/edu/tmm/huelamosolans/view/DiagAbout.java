/*
 * DiagAbout.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.view;

/**
 * 
 * @author albertohuelamosegura
 */
public class DiagAbout extends javax.swing.JDialog {

    private static final long serialVersionUID = 5090816687174324006L;

    /**
     * Creates new form DiagAbout
     */
    public DiagAbout(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIcon = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblIcon.setIcon(new javax.swing.ImageIcon(
                getClass()
                        .getResource(
                                "/ub/edu/tmm/huelamosolans/view/10004_Universitat_de_Barcelona.gif"))); // NOI18N
        getContentPane().add(lblIcon, java.awt.BorderLayout.LINE_START);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel1.setText("Practica Tecnologies Multimedia");

        jLabel2.setText("David Solans Noguero");

        jLabel3.setText("Alberto Huelamo Segura");

        jLabel4.setText("Curs 2012-2013, Universitat de Barcelona");

        jButton1.setText("Close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(
                jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout
                .setHorizontalGroup(jPanel1Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel1Layout
                                .createSequentialGroup()
                                .addContainerGap()
                                .add(jPanel1Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanel1Layout
                                                .createSequentialGroup()
                                                .add(jLabel1)
                                                .add(0, 0, Short.MAX_VALUE))
                                        .add(jPanel1Layout
                                                .createSequentialGroup()
                                                .add(jPanel1Layout
                                                        .createParallelGroup(
                                                                org.jdesktop.layout.GroupLayout.LEADING)
                                                        .add(jLabel2)
                                                        .add(jLabel3))
                                                .addPreferredGap(
                                                        org.jdesktop.layout.LayoutStyle.RELATED,
                                                        70, Short.MAX_VALUE)
                                                .add(jButton1)))
                                .addContainerGap())
                        .add(jPanel1Layout.createSequentialGroup().add(jLabel4)
                                .add(0, 49, Short.MAX_VALUE)));
        jPanel1Layout
                .setVerticalGroup(jPanel1Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel1Layout
                                .createSequentialGroup()
                                .add(18, 18, 18)
                                .add(jLabel1)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jPanel1Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanel1Layout
                                                .createSequentialGroup()
                                                .add(jLabel2)
                                                .addPreferredGap(
                                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                                .add(jLabel3)).add(jButton1))
                                .add(18, 18, 18).add(jLabel4)
                                .addContainerGap(28, Short.MAX_VALUE)));

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }// GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[]) {

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblIcon;
    // End of variables declaration//GEN-END:variables
}
