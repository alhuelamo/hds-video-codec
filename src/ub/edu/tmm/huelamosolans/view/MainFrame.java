/*
 * MainFrame.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;

import tsm.practiques.ImageEditor;
import tsm.practiques.pTSM;
import ub.edu.tmm.huelamosolans.codec.ConcurrentDecoder;
import ub.edu.tmm.huelamosolans.codec.ConcurrentEncoder;
import ub.edu.tmm.huelamosolans.codec.Video;
import ub.edu.tmm.huelamosolans.zip.ZipLoader;

/**
 * Main view class.
 * Contains whole main GUI.
 * 
 * @author albertohuelamosegura
 */
public class MainFrame extends javax.swing.JFrame implements ActionListener {

    private static final long serialVersionUID = -5375008498814204897L;
    public static final int DELAY = 40;
    private static final String TEXT_EFFECT_APPLIED = "Effect applied: ";
    private ZipLoader loader;
    private List<pTSM> images;
    private int counter;
    private ImageIcon icon;
    private Timer timer;
    private ImageEditor filters;

    // Compression parameters
    int tWidth = 32;
    int tHeight = 32;
    int baseCount = 10;
    int displacement = 4;
    int nTurns = 3;
    int averageTolerance = 20;

    /**
     * Creates new form.
     */
    public MainFrame() {
        loader = new ZipLoader();
        timer = new Timer(DELAY, this);
        initComponents();
        disableAll();
    }

    /**
     * Sets the maximum and the minimum value of the progress bar.
     * It DOES NOT modify the size of the widget.
     * 
     * @param min
     *            Minimum value.
     * @param max
     *            Maximum value.
     */
    public void setBoundsProgressBar(int min, int max) {
        progressBar.setMinimum(min);
        progressBar.setMaximum(max);
    }

    /**
     * Sets fill value of the progress bar.
     * 
     * @param value
     *            Fill value of the progress bar.
     */
    public void setProgressBarValue(int value) {
        progressBar.setValue(value);
    }

    /**
     * Shows a message dialog with MainFrame as parent component.
     * 
     * @param message
     *            Message.
     * @param title
     *            Dialog title.
     * @param type
     *            Type message. See JOptionPane.
     */
    public void messageDialog(String message, String title, int type) {
        JOptionPane.showMessageDialog(this, message, title, type);
    }

    /**
     * Prepares the GUI in order to visualize an image sequence.
     * 
     * @param images
     *            List of images.
     */
    public void setImages(List<pTSM> images) {
        this.images = images;
        loader.setImages(images);
        counter = 0;
        updateImage();
        enableAll();
        filters = new ImageEditor(images);
        resize();
    }

    /**
     * Timer event handler.
     * Shows the next image.
     * 
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        nextImage();
    }

    /**
     * Refreshes image widget.
     */
    private void updateImage() {
        icon = new ImageIcon(images.get(counter).getBufImg());
        lblImage.setIcon(icon);
    }

    /**
     * Refreshes image widget.
     * 
     * @param p
     *            Image sequence index.
     */
    private void updateImage(int p) {
        counter = p;
        updateImage();
    }

    /**
     * Shows next image sequence.
     */
    private void nextImage() {
        counter = (counter + 1) % images.size();
        updateImage();
    }

    /**
     * Shows previous image sequence.
     */
    private void prevImage() {
        counter = (counter - 1);
        if (counter < 0) {
            counter = images.size() - 1;
        }
        updateImage();
    }

    /**
     * Sets the text of the filter label.
     * 
     * @param text
     *            New filter text.
     */
    private void textLabelEffect(String text) {
        lblEffect.setText(TEXT_EFFECT_APPLIED + text);
    }

    /**
     * Enables or disables all controls.
     * 
     * @param act
     *            True if enabled, false if not.
     */
    private void activateControls(boolean act) {
        btnNext.setEnabled(act);
        btnPrev.setEnabled(act);
        btnPlay.setEnabled(act);
        rdBCh.setEnabled(act);
        rdBrighness.setEnabled(act);
        rdGCh.setEnabled(act);
        rdGrayScale.setEnabled(act);
        rdInverse.setEnabled(act);
        rdOriginal.setEnabled(act);
        rdRCh.setEnabled(act);
        rdThreshold.setEnabled(act);
        txtBrightness.setEnabled(act);
        txtThreshold.setEnabled(act);
        slidBrightness.setEnabled(act);
        slidThreshold.setEnabled(act);
    }

    /**
     * Enables controls for use.
     * Disables radio button controls.
     */
    private void enableAll() {
        activateControls(true);
        rdOriginal.setSelected(true);
        rdOriginalActionPerformed(null);
        txtBrightness.setEnabled(false);
        txtThreshold.setEnabled(false);
        slidBrightness.setEnabled(false);
        slidThreshold.setEnabled(false);
    }

    /**
     * Disables all controls.
     */
    private void disableAll() {
        activateControls(false);
    }

    /**
     * Disables threshold and brightness auxiliar controls.
     */
    private void disableBrightnessAndThreshold() {
        slidBrightness.setEnabled(false);
        txtBrightness.setEnabled(false);
        slidThreshold.setEnabled(false);
        txtThreshold.setEnabled(false);
    }

    /**
     * Resizes window if the image is too big.
     */
    private void resize() {
        BufferedImage im = images.get(0).getBufImg();
        int x = im.getWidth(), y = im.getHeight();
        if (x > lblImage.getWidth()) {
            setSize(x + 6, getHeight());
        } else if (y > lblImage.getHeight()) {
            setSize(getWidth(), y + 6);
        }
    }

    /**
     * Finishes the program.
     */
    private void exit() {
        System.exit(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtPath = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btnLoadVideo = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        rdOriginal = new javax.swing.JRadioButton();
        rdInverse = new javax.swing.JRadioButton();
        rdGrayScale = new javax.swing.JRadioButton();
        rdRCh = new javax.swing.JRadioButton();
        rdGCh = new javax.swing.JRadioButton();
        rdBCh = new javax.swing.JRadioButton();
        rdBrighness = new javax.swing.JRadioButton();
        slidBrightness = new javax.swing.JSlider();
        txtBrightness = new javax.swing.JTextField();
        rdThreshold = new javax.swing.JRadioButton();
        slidThreshold = new javax.swing.JSlider();
        txtThreshold = new javax.swing.JTextField();
        lblEffect = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();
        btnPlay = new javax.swing.JButton();
        btnPrev = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuExit = new javax.swing.JMenuItem();
        menuTools = new javax.swing.JMenu();
        menuPreferences = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenu3.setText("File");
        jMenuBar2.add(jMenu3);

        jMenu4.setText("Edit");
        jMenuBar2.add(jMenu4);

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PracticaTMM");
        addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }

            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory
                .createTitledBorder("File options"));

        jLabel1.setText("Input file:");

        txtPath.setFocusable(false);

        jButton1.setText("Load ZIP");
        jButton1.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnSave.setText("Save...");
        btnSave.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jButton2.setText("Save video...");
        jButton2.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btnLoadVideo.setText("Load video");
        btnLoadVideo.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadVideoActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(
                jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout
                .setHorizontalGroup(jPanel1Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING,
                                jPanel1Layout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .add(jPanel1Layout
                                                .createParallelGroup(
                                                        org.jdesktop.layout.GroupLayout.LEADING,
                                                        false)
                                                .add(jPanel1Layout
                                                        .createSequentialGroup()
                                                        .add(jLabel1)
                                                        .addPreferredGap(
                                                                org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(txtPath,
                                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                393,
                                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                                .add(progressBar,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE))
                                        .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jPanel1Layout
                                                .createParallelGroup(
                                                        org.jdesktop.layout.GroupLayout.LEADING,
                                                        false)
                                                .add(btnLoadVideo,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .add(jButton1,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE))
                                        .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(jPanel1Layout
                                                .createParallelGroup(
                                                        org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(jButton2,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        177, Short.MAX_VALUE)
                                                .add(btnSave,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE))
                                        .addContainerGap()));
        jPanel1Layout
                .setVerticalGroup(jPanel1Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel1Layout
                                .createSequentialGroup()
                                .addContainerGap()
                                .add(jPanel1Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(jLabel1)
                                        .add(txtPath,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(jButton1).add(btnSave))
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel1Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING,
                                                false)
                                        .add(jPanel1Layout
                                                .createParallelGroup(
                                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                                .add(jButton2,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .add(btnLoadVideo,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE))
                                        .add(progressBar,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE))
                                .addContainerGap(13, Short.MAX_VALUE)));

        jPanel2.setBorder(javax.swing.BorderFactory
                .createTitledBorder("Image processing"));

        jPanel3.setBorder(javax.swing.BorderFactory
                .createTitledBorder("Effects"));

        buttonGroup1.add(rdOriginal);
        rdOriginal.setText("Original");
        rdOriginal.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdOriginalActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdInverse);
        rdInverse.setText("Inverse");
        rdInverse.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdInverseActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdGrayScale);
        rdGrayScale.setText("Gray scale");
        rdGrayScale.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdGrayScaleActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdRCh);
        rdRCh.setText("R channel");
        rdRCh.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdRChActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdGCh);
        rdGCh.setText("G channel");
        rdGCh.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdGChActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdBCh);
        rdBCh.setText("B channel");
        rdBCh.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBChActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdBrighness);
        rdBrighness.setText("Brightness");
        rdBrighness.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBrighnessActionPerformed(evt);
            }
        });

        slidBrightness.setMaximum(25);
        slidBrightness.setMinimum(-25);
        slidBrightness.setValue(0);
        slidBrightness.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                slidBrightnessMouseReleased(evt);
            }
        });

        txtBrightness.setFocusable(false);

        @SuppressWarnings("rawtypes")
        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings
                .createAutoBinding(
                        org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE,
                        slidBrightness,
                        org.jdesktop.beansbinding.ELProperty.create("${value}"),
                        txtBrightness,
                        org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        buttonGroup1.add(rdThreshold);
        rdThreshold.setText("Threshold B&W");
        rdThreshold.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdThresholdActionPerformed(evt);
            }
        });

        slidThreshold.setMaximum(255);
        slidThreshold.setValue(0);
        slidThreshold.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                slidThresholdMouseReleased(evt);
            }
        });

        txtThreshold.setFocusable(false);

        binding = org.jdesktop.beansbinding.Bindings
                .createAutoBinding(
                        org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE,
                        slidThreshold,
                        org.jdesktop.beansbinding.ELProperty.create("${value}"),
                        txtThreshold,
                        org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblEffect.setText("Effect applied: ");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(
                jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout
                .setHorizontalGroup(jPanel3Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel3Layout
                                .createSequentialGroup()
                                .addContainerGap()
                                .add(jPanel3Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanel3Layout
                                                .createSequentialGroup()
                                                .add(slidBrightness,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        127,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(
                                                        org.jdesktop.layout.LayoutStyle.RELATED,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .add(txtBrightness,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        43,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(jPanel3Layout
                                                .createSequentialGroup()
                                                .add(jPanel3Layout
                                                        .createParallelGroup(
                                                                org.jdesktop.layout.GroupLayout.LEADING)
                                                        .add(rdOriginal)
                                                        .add(rdInverse)
                                                        .add(rdGrayScale)
                                                        .add(rdRCh)
                                                        .add(rdGCh)
                                                        .add(rdBCh)
                                                        .add(rdBrighness)
                                                        .add(rdThreshold)
                                                        .add(jPanel3Layout
                                                                .createSequentialGroup()
                                                                .add(slidThreshold,
                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                        127,
                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                                                .add(txtThreshold,
                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                        43,
                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                                        .add(lblEffect))
                                                .add(0, 0, Short.MAX_VALUE)))
                                .addContainerGap()));
        jPanel3Layout
                .setVerticalGroup(jPanel3Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel3Layout
                                .createSequentialGroup()
                                .addContainerGap()
                                .add(rdOriginal)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdInverse)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdGrayScale)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdRCh)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdGCh)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdBCh)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdBrighness)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel3Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(slidBrightness,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(txtBrightness,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(rdThreshold)
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel3Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(slidThreshold,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(txtThreshold,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        org.jdesktop.layout.LayoutStyle.RELATED,
                                        11, Short.MAX_VALUE).add(lblEffect)
                                .addContainerGap()));

        lblImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblImage.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnPlay.setText("Play/Pause");
        btnPlay.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayActionPerformed(evt);
            }
        });

        btnPrev.setText("Previous");
        btnPrev.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrevActionPerformed(evt);
            }
        });

        btnNext.setText("Next");
        btnNext.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(
                jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout
                .setHorizontalGroup(jPanel2Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING,
                                jPanel2Layout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .add(lblImage,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                        .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jPanel2Layout
                                                .createParallelGroup(
                                                        org.jdesktop.layout.GroupLayout.LEADING,
                                                        false)
                                                .add(jPanel3,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .add(btnPlay,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .add(jPanel2Layout
                                                        .createSequentialGroup()
                                                        .add(btnPrev)
                                                        .addPreferredGap(
                                                                org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(btnNext,
                                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                Short.MAX_VALUE)))
                                        .addContainerGap()));
        jPanel2Layout
                .setVerticalGroup(jPanel2Layout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel2Layout
                                .createSequentialGroup()
                                .addContainerGap()
                                .add(jPanel2Layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(lblImage,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                        .add(jPanel2Layout
                                                .createSequentialGroup()
                                                .add(jPanel3,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(
                                                        org.jdesktop.layout.LayoutStyle.RELATED)
                                                .add(btnPlay)
                                                .addPreferredGap(
                                                        org.jdesktop.layout.LayoutStyle.RELATED,
                                                        12, Short.MAX_VALUE)
                                                .add(jPanel2Layout
                                                        .createParallelGroup(
                                                                org.jdesktop.layout.GroupLayout.BASELINE)
                                                        .add(btnPrev)
                                                        .add(btnNext))))));

        jMenu1.setText("File");

        menuExit.setText("Exit");
        menuExit.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExitActionPerformed(evt);
            }
        });
        jMenu1.add(menuExit);

        jMenuBar1.add(jMenu1);

        menuTools.setText("Tools");

        menuPreferences.setText("Preferences...");
        menuPreferences.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPreferencesActionPerformed(evt);
            }
        });
        menuTools.add(menuPreferences);

        jMenuBar1.add(menuTools);

        jMenu2.setText("Help");

        jMenuItem1.setText("About...");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING,
                        layout.createSequentialGroup()
                                .addContainerGap()
                                .add(layout
                                        .createParallelGroup(
                                                org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanel2,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                        .add(jPanel1,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE))
                                .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING).add(
                layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jPanel1,
                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2,
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE).addContainerGap()));

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
        Logger.getLogger(getClass().getName()).log(Level.INFO,
                "Begin loading images.");
        JFileChooser fc = new JFileChooser();
        // fc.setFileFilter(new FileNameExtensionFilter("BMP, JPG, PNG, GIF",
        // "bmp", "jpg","png","gif"));
        fc.showOpenDialog(this);
        File file = fc.getSelectedFile();
        if (file == null) {
            JOptionPane.showMessageDialog(this, "Input cancelled!", "Warning!",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                loader = new ZipLoader();
                counter = 0;
                String path = file.getPath();
                txtPath.setText(path);

                loader.load(path);
                images = loader.getImages();
                updateImage();
                enableAll();
                filters = new ImageEditor(images);
                resize();
            } catch (IOException ex) {}
        }
    }// GEN-LAST:event_jButton1ActionPerformed

    private void rdThresholdActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdThresholdActionPerformed
        timer.stop();
        textLabelEffect(rdThreshold.getText());
        slidBrightness.setEnabled(false);
        txtBrightness.setEnabled(false);
        slidThreshold.setEnabled(true);
        txtThreshold.setEnabled(true);

        // images=filters.thresholdEffect(slidThreshold.getValue());
        images = filters.thresholdEffect(100);
        updateImage(0);
    }// GEN-LAST:event_rdThresholdActionPerformed

    private void rdOriginalActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdOriginalActionPerformed
        timer.stop();
        textLabelEffect(rdOriginal.getText());
        images = loader.getImages();
        disableBrightnessAndThreshold();
        updateImage(0);
    }// GEN-LAST:event_rdOriginalActionPerformed

    private void rdInverseActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdInverseActionPerformed
        timer.stop();
        textLabelEffect(rdGrayScale.getText());
        disableBrightnessAndThreshold();
        images = filters.inverseEffect();
        updateImage(0);
    }// GEN-LAST:event_rdInverseActionPerformed

    private void rdGrayScaleActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdGrayScaleActionPerformed
        timer.stop();
        textLabelEffect(rdGrayScale.getText());
        disableBrightnessAndThreshold();
        images = filters.grayScaleEffect();
        updateImage(0);
    }// GEN-LAST:event_rdGrayScaleActionPerformed

    private void rdRChActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdRChActionPerformed
        timer.stop();
        textLabelEffect(rdRCh.getText());
        disableBrightnessAndThreshold();
        images = filters.rChannelEffect();
        updateImage(0);
    }// GEN-LAST:event_rdRChActionPerformed

    private void rdGChActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdGChActionPerformed
        timer.stop();
        textLabelEffect(rdGCh.getText());
        disableBrightnessAndThreshold();
        images = filters.gChannelEffect();
        updateImage(0);
    }// GEN-LAST:event_rdGChActionPerformed

    private void rdBChActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdBChActionPerformed
        timer.stop();
        textLabelEffect(rdBCh.getText());
        disableBrightnessAndThreshold();
        images = filters.bChannelEffect();
        updateImage(0);
    }// GEN-LAST:event_rdBChActionPerformed

    private void rdBrighnessActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_rdBrighnessActionPerformed
        timer.stop();
        textLabelEffect(rdBrighness.getText());
        slidBrightness.setEnabled(true);
        txtBrightness.setEnabled(true);
        slidThreshold.setEnabled(false);
        txtThreshold.setEnabled(false);

        images = filters.brightnessEffect(slidBrightness.getValue());
        // images=filters.brightnessEffect(80);
        updateImage(0);
    }// GEN-LAST:event_rdBrighnessActionPerformed

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnNextActionPerformed
        timer.stop();
        nextImage();
    }// GEN-LAST:event_btnNextActionPerformed

    private void btnPrevActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnPrevActionPerformed
        timer.stop();
        prevImage();
    }// GEN-LAST:event_btnPrevActionPerformed

    private void btnPlayActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnPlayActionPerformed
        if (timer.isRunning()) {
            timer.stop();
        } else {
            timer.start();
        }
    }// GEN-LAST:event_btnPlayActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuItem1ActionPerformed
        new DiagAbout(this, true).setVisible(true);
    }// GEN-LAST:event_jMenuItem1ActionPerformed

    private void menuExitActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuExitActionPerformed
        exit();
    }// GEN-LAST:event_menuExitActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSaveActionPerformed
        JFileChooser fc = new JFileChooser();
        fc.showSaveDialog(this);
        File file = fc.getSelectedFile();
        if (file == null) {
            JOptionPane.showMessageDialog(this, "Output cancelled!",
                    "Warning!", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                String path = file.getPath();
                List<BufferedImage> buf = new ArrayList<BufferedImage>();
                for (pTSM im : images) {
                    buf.add(im.getBufImg());
                }
                Video video = new Video(buf, null);
                video.saveToFile(path, 0, 0, 0);
                JOptionPane.showMessageDialog(this, "File saved.", "Info",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (FileNotFoundException ex) {} catch (IOException ex) {}
        }
    }// GEN-LAST:event_btnSaveActionPerformed

    private void slidBrightnessMouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_slidBrightnessMouseReleased
        images = filters.brightnessEffect(slidBrightness.getValue());
        updateImage(0);
    }// GEN-LAST:event_slidBrightnessMouseReleased

    private void slidThresholdMouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_slidThresholdMouseReleased
        images = filters.thresholdEffect(slidThreshold.getValue());
        updateImage(0);
    }// GEN-LAST:event_slidThresholdMouseReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosed
        exit();
    }// GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
        exit();
    }// GEN-LAST:event_formWindowClosing

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton2ActionPerformed
        Logger.getLogger(getClass().getName()).log(Level.INFO,
                "Save option selected.");
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("HDS", "hds"));
        fc.showSaveDialog(this);
        File file = fc.getSelectedFile();
        if (file == null) {
            JOptionPane.showMessageDialog(this, "Output cancelled!",
                    "Warning!", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                String path = file.getPath();
                path += ".hds";
                ConcurrentEncoder.encodeAndSave(images, tWidth, tHeight,
                        baseCount, displacement, nTurns, averageTolerance,
                        this, path);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE,
                        null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE,
                        null, ex);
            }
        }
    }// GEN-LAST:event_jButton2ActionPerformed

    private void btnLoadVideoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnLoadVideoActionPerformed
        Logger.getLogger(getClass().getName()).log(Level.INFO,
                "Load video option selected.");
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("HDS", "hds"));
        fc.showOpenDialog(this);
        File file = fc.getSelectedFile();
        if (file == null) {
            JOptionPane.showMessageDialog(this, "Output cancelled!",
                    "Warning!", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                String path = file.getPath();
                ConcurrentDecoder.loadAndDecode(this, path);
            } catch (ClassNotFoundException ex) {} catch (FileNotFoundException ex) {} catch (IOException ex) {}
        }
    }// GEN-LAST:event_btnLoadVideoActionPerformed

    private void menuPreferencesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuPreferencesActionPerformed
        DiagOptions diag = new DiagOptions(this, true);
        diag.setVisible(true);
    }// GEN-LAST:event_menuPreferencesActionPerformed

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed"
        // desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel.
         * For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel
         * /plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
                    .getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        }
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLoadVideo;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnPrev;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblEffect;
    private javax.swing.JLabel lblImage;
    private javax.swing.JMenuItem menuExit;
    private javax.swing.JMenuItem menuPreferences;
    private javax.swing.JMenu menuTools;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JRadioButton rdBCh;
    private javax.swing.JRadioButton rdBrighness;
    private javax.swing.JRadioButton rdGCh;
    private javax.swing.JRadioButton rdGrayScale;
    private javax.swing.JRadioButton rdInverse;
    private javax.swing.JRadioButton rdOriginal;
    private javax.swing.JRadioButton rdRCh;
    private javax.swing.JRadioButton rdThreshold;
    private javax.swing.JSlider slidBrightness;
    private javax.swing.JSlider slidThreshold;
    private javax.swing.JTextField txtBrightness;
    private javax.swing.JTextField txtPath;
    private javax.swing.JTextField txtThreshold;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
