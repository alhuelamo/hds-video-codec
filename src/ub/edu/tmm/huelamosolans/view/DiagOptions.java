/*
 * DiagOptions.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.view;

/**
 * 
 * @author albertohuelamosegura
 */
public class DiagOptions extends javax.swing.JDialog {

    private static final long serialVersionUID = 7140276986458890L;
    private MainFrame control;

    /**
     * Creates new form DiagOptions
     */
    public DiagOptions(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        control = (MainFrame) parent;
        txtAverageTolerance.setText(Integer.toString(control.averageTolerance));
        txtBaseImage.setText(Integer.toString(control.baseCount));
        txtDisplacement.setText(Integer.toString(control.displacement));
        txtHeigth.setText(Integer.toString(control.tHeight));
        txtNTurns.setText(Integer.toString(control.nTurns));
        txtWidth.setText(Integer.toString(control.tWidth));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtWidth = new javax.swing.JTextField();
        txtHeigth = new javax.swing.JTextField();
        txtBaseImage = new javax.swing.JTextField();
        txtNTurns = new javax.swing.JTextField();
        txtDisplacement = new javax.swing.JTextField();
        txtAverageTolerance = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLbl2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);

        txtWidth.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtWidthFocusLost(evt);
            }
        });

        txtHeigth.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHeigthFocusLost(evt);
            }
        });

        txtBaseImage.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtBaseImageFocusLost(evt);
            }
        });

        txtNTurns.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNTurnsFocusLost(evt);
            }
        });

        txtDisplacement.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDisplacementFocusLost(evt);
            }
        });

        txtAverageTolerance.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtAverageToleranceFocusLost(evt);
            }
        });

        jLabel1.setText("Height of the tessels:");

        jLbl2.setText("Width of the tessels:");

        jLabel3.setText("Base frame counter: ");

        jLabel4.setText("Search turns: ");

        jLabel5.setText("Search displacement: ");

        jLabel6.setText("Average tolerance: ");

        btnOK.setText("Close");
        btnOK.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(btnOK).add(jLbl2).add(jLabel1)
                                .add(jLabel3).add(jLabel4).add(jLabel5)
                                .add(jLabel6))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.LEADING)
                                .add(txtAverageTolerance,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtDisplacement,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtNTurns,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtBaseImage,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtHeigth,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtWidth,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        45,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE)));
        layout.setVerticalGroup(layout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(txtWidth,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLbl2,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(txtHeigth,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabel1,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.LEADING)
                                .add(txtBaseImage,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabel3,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(txtNTurns,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabel4,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(txtDisplacement,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabel5,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(txtAverageTolerance,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabel6,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)).add(18, 18, 18)
                        .add(btnOK).add(12, 12, 12)));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnOKActionPerformed
        control.averageTolerance = Integer.parseInt(txtAverageTolerance
                .getText());
        control.baseCount = Integer.parseInt(txtBaseImage.getText());
        control.displacement = Integer.parseInt(txtDisplacement.getText());
        control.nTurns = Integer.parseInt(txtNTurns.getText());
        control.tHeight = Integer.parseInt(txtHeigth.getText());
        control.tWidth = Integer.parseInt(txtWidth.getText());
        dispose();
    }// GEN-LAST:event_btnOKActionPerformed

    private void txtWidthFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtWidthFocusLost
        try {
            control.tWidth = Integer.parseInt(txtWidth.getText());
        } catch (NumberFormatException ex) {
            txtWidth.setText(Integer.toString(control.tWidth));
        }
    }// GEN-LAST:event_txtWidthFocusLost

    private void txtHeigthFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtHeigthFocusLost
        try {
            control.tHeight = Integer.parseInt(txtHeigth.getText());
        } catch (NumberFormatException ex) {
            txtHeigth.setText(Integer.toString(control.tHeight));
        }
    }// GEN-LAST:event_txtHeigthFocusLost

    private void txtBaseImageFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtBaseImageFocusLost
        try {
            control.baseCount = Integer.parseInt(txtBaseImage.getText());
        } catch (NumberFormatException ex) {
            txtBaseImage.setText(Integer.toString(control.baseCount));
        }
    }// GEN-LAST:event_txtBaseImageFocusLost

    private void txtNTurnsFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtNTurnsFocusLost
        try {
            control.nTurns = Integer.parseInt(txtNTurns.getText());
        } catch (NumberFormatException ex) {
            txtNTurns.setText(Integer.toString(control.nTurns));
        }
    }// GEN-LAST:event_txtNTurnsFocusLost

    private void txtDisplacementFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtDisplacementFocusLost
        try {
            control.displacement = Integer.parseInt(txtDisplacement.getText());
        } catch (NumberFormatException ex) {
            txtDisplacement.setText(Integer.toString(control.displacement));
        }
    }// GEN-LAST:event_txtDisplacementFocusLost

    private void txtAverageToleranceFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_txtAverageToleranceFocusLost
        try {
            control.averageTolerance = Integer.parseInt(txtAverageTolerance
                    .getText());
        } catch (NumberFormatException ex) {
            txtAverageTolerance.setText(Integer
                    .toString(control.averageTolerance));
        }
    }// GEN-LAST:event_txtAverageToleranceFocusLost

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[]) {

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLbl2;
    private javax.swing.JTextField txtAverageTolerance;
    private javax.swing.JTextField txtBaseImage;
    private javax.swing.JTextField txtDisplacement;
    private javax.swing.JTextField txtHeigth;
    private javax.swing.JTextField txtNTurns;
    private javax.swing.JTextField txtWidth;
    // End of variables declaration//GEN-END:variables
}
