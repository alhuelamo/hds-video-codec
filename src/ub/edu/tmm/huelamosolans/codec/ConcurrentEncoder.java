/*
 * ConcurrentEncoder.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

 package ub.edu.tmm.huelamosolans.codec;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import tsm.practiques.pTSM;
import ub.edu.tmm.huelamosolans.view.MainFrame;

/**
 * Independent thread encoder.
 * Made in order to interact with view class and not to block the GUI.
 * @author albertohuelamosegura
 */
public class ConcurrentEncoder extends Encoder implements Runnable {
    
    private MainFrame view;
    private String savePath;
    
    /**
     * Sole constructor.
     * @param images Input image sequence.
     * @param tWidth Tessel width.
     * @param tHeight Tessel height.
     * @param baseCount Base frame index.
     * @param displacement Search displacement distance in pixels.
     * @param nTurns Search spiral turns number.
     * @param averageTolerance Tolerance to compare two tessels.
     * @param view Main view class.
     * @param savePath Path to save the encoded video.
     */
    public ConcurrentEncoder(List<pTSM> images, int tWidth, int tHeight, int baseCount,
            int displacement, int nTurns, double averageTolerance, MainFrame view, String savePath) {
        super(images, tWidth, tHeight, baseCount,
                displacement, nTurns, averageTolerance);
        this.view = view;
        this.savePath = savePath;
    }
    
    @Override
    public void encode() {
        pTSM frame, prevFrame;
        Tessel tessel;
        MatchVector match;
        BufferedImage compFrame;
        List<Tessel> tessels;
        
        view.setBoundsProgressBar(0, input.size());
        
        for (int i = 0; i < input.size(); i++) {
            frame = input.get(i);
            if (!isImageBase(i)) {
                prevFrame = input.get(i - 1);
                tessels = tessellate(prevFrame);
                compFrame = frame.getBufImg();
                for (int j = 0; j < tessels.size(); j++) {
                    tessel = tessels.get(j);
                    //seek
                    match = searchMatches(frame, tessel, j);
                    //compensate
                    if(match != null) {
                        match.nFrame = i;
                        compFrame = compensate(compFrame, tessel, match);
                        output.addVector(match);
                    }
                }
                output.addFrame(compFrame);
            } else {
                output.addFrame(frame.getBufImg());
            }
            view.setProgressBarValue(i+1);
        }
    }

    @Override
    public void run() {
        long t1 = System.nanoTime();
        encode();
        long t2 = System.nanoTime();
        try {
            output.saveToFile(savePath, tHeight, tWidth, baseCount);
        } catch (IOException ex) {
            Logger.getLogger(ConcurrentEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        String message = "Video saved in " + (float)(t2 - t1) / 1000000000.0 + " seconds.";
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, message);
        view.messageDialog(message,
                "Info", JOptionPane.INFORMATION_MESSAGE);
    }
    
    /**
     * Engages all the concurrent encodig process.
     * @param images Input image sequence.
     * @param tWidth Tessel width.
     * @param tHeight Tessel height.
     * @param baseCount Base frame index.
     * @param displacement Search displacement distance in pixels.
     * @param nTurns Search spiral turns number.
     * @param averageTolerance Tolerance to compare two tessels.
     * @param view Main view class.
     * @param path Path to save the encoded video.
     * @throws IOException Thrown if there is a problem writing the file.
     */
    public static void encodeAndSave(List<pTSM> images, int tWidth, int tHeight,
            int baseCount, int displacement, int nTurns,
            double averageTolerance, MainFrame view, String path) throws IOException {
        ConcurrentEncoder encoder = new ConcurrentEncoder(images, tWidth,
                tHeight, baseCount, displacement, nTurns, averageTolerance,
                view, path);
        new Thread(encoder).start();
    }
    
}
