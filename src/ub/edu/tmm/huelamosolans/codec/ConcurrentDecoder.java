/*
 * ConcurrentDecoder.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.codec;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import tsm.practiques.pTSM;
import ub.edu.tmm.huelamosolans.view.MainFrame;

/**
 * Independent thread decoder.
 * Made in order to interact with view class and not to block the GUI.
 */
public class ConcurrentDecoder extends Decoder implements Runnable {

    private MainFrame view;
    private List<pTSM> out;
    
    /**
     * Sole constructor.
     * @param path Path to load de video file.
     * @param view Main view class.
     * @throws IOException Thrown if the file does not exist or there is a problem reading it.
     * @throws ClassNotFoundException Thrown if the file is corrupt.
     */
    public ConcurrentDecoder(String path, MainFrame view)
            throws IOException, ClassNotFoundException {
        super(path);
        this.view = view;
    }
    
    @Override
    public void decode() {
        BufferedImage frame, prevFrame;
        List<Tessel> tessels;
        List<MatchVector> vecs;
        Tessel tessel;
        
        view.setBoundsProgressBar(0, input.getFrames().size());
        
        for(int nFrame = 0; nFrame < input.getFrames().size(); nFrame++) {
            frame = input.getFrame(nFrame);
            if(!isImageBase(nFrame)) {
                prevFrame = input.getFrame(nFrame - 1);
                vecs = input.getVectorsForFrame(nFrame);
                if(vecs.size() > 0) {
                    tessels = tessellate(prevFrame.getRaster());
                    for(MatchVector v: vecs) {
                        tessel = tessels.get(v.nTessel);
                        replaceForTessel(frame, tessel, v);
                    }
                }
                output.addFrame(frame);
            } else {
                output.addFrame(frame);
            }
            view.setProgressBarValue(nFrame+1);
        }
    }
    
    @Override
    public void run() {
        long t1 = System.nanoTime();
        decode();
        long t2 = System.nanoTime();
        List<BufferedImage> dec = output.getFrames();
        out = new ArrayList<pTSM>();
        for(BufferedImage im: dec) {
            out.add(new pTSM(im, ""));
        }
        view.setImages(out);
        String message = "Video loaded in " + (float)(t2 - t1) / 1000000000.0 + " seconds.";
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, message);
        view.messageDialog(message,
                "Info", JOptionPane.INFORMATION_MESSAGE);
    }
    
    /**
     * Engages all the concurrent decoding process.
     * @param view Main view class.
     * @param path Path to load de video file.
     * @throws IOException Thrown if the file does not exist or there is a problem reading it.
     * @throws ClassNotFoundException Thrown if the file is corrupt.
     */
    public static void loadAndDecode(MainFrame view, String path)
            throws IOException, ClassNotFoundException {
        ConcurrentDecoder decoder = new ConcurrentDecoder(path, view);
        new Thread(decoder).start();
    }
    
}
