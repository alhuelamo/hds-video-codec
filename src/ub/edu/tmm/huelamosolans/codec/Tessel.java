/*
 * Tessel.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.codec;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

/**
 * A portion of an image.
 * @author Alberto
 */
public class Tessel {

    private int nTessel;
    private WritableRaster raster;
    private int x;
    private int y;

    /**
     * Sole constructor.
     * @param nTessel Tessel number in a frame.
     * @param raster Tessel raster.
     * @param x X coordinate in a frame.
     * @param y Y coordinate in a frame.
     */
    public Tessel(int nTessel, WritableRaster raster, int x, int y) {
        this.nTessel = nTessel;
        this.raster = raster;
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the number of the tessel in a frame.
     * @return The number of the tessel in a frame.
     */
    public int getnTessel() {
        return nTessel;
    }

    /**
     * Gets the tessel raster.
     * @return The tessel raster.
     */
    public WritableRaster getRaster() {
        return raster;
    }

    /**
     * Gets the X tessel coordinate in a frame.
     * @return X tessel coordinate in a frame.
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the Y tessel coordinate in a frame.
     * @return X tessel coordinate in a frame.
     */
    public int getY() {
        return y;
    }
    
    /**
     * Gets the BufferedImage object corresponding to the tessel.
     * @return Tessel on BufferedImage form.
     */
    public BufferedImage createBufferedImage() {
        return new BufferedImage(ColorModel.getRGBdefault(), raster, true, null);
    }

}
