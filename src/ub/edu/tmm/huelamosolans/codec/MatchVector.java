/*
 * MatchVector.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.codec;

import java.io.Serializable;

/**
 * Stores information about a tessel position in other frame.
 * 
 * @author albertohuelamosegura
 */
public class MatchVector implements Serializable {

    private static final long serialVersionUID = 2535958176597203L;

    public int x;
    public int y;
    public int nTessel;
    public int nFrame;

    /**
     * Parametrized constructor.
     * 
     * @param x
     *            X tessel coordinate in a frame.
     * @param y
     *            Y tessel coordinate in a frame.
     * @param nTessel
     *            Tessel index.
     */
    public MatchVector(int x, int y, int nTessel) {
        this.x = x;
        this.y = y;
        this.nTessel = nTessel;
    }

    /**
     * Parametrized constructor.
     * 
     * @param x
     *            X tessel coordinate in a frame.
     * @param y
     *            Y tessel coordinate in a frame.
     * @param nTessel
     *            Tessel index.
     * @param nFrame
     *            Frame index.
     */
    public MatchVector(int x, int y, int nTessel, int nFrame) {
        this.x = x;
        this.y = y;
        this.nTessel = nTessel;
        this.nFrame = nFrame;
    }

    /**
     * toString override.
     * 
     * @return String with vector information.
     */
    @Override
    public String toString() {
        return x + " " + y + " " + nTessel + " " + nFrame;
    }

    /**
     * Equals override.
     * 
     * @param o
     *            Other object.
     * @return True if equals, false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MatchVector)) {
            return false;
        }
        MatchVector other = (MatchVector) o;
        if (x == other.x && y == other.y && nTessel == other.nTessel
                && nFrame == other.nFrame) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Override hash code.
     * 
     * @return Hash code.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + x;
        hash = 31 * hash + y;
        hash = 31 * hash + nTessel;
        hash = 31 * hash + nFrame;
        return hash;
    }

}
