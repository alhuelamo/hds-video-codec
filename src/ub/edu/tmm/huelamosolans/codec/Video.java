/*
 * Video.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.codec;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.imageio.ImageIO;

/**
 * Video object.
 * Stores frame sequence, a set of match vectors, the tessel measures and
 * base image index.
 * @author Alberto
 */
public class Video {
    
    private List<BufferedImage> frames;
    private List<MatchVector> vectors;
    private int tHeight;
    private int tWidth;
    private int baseCount;
    
    /**
     * No parametrized constructor.
     */
    public Video() {
        frames = new ArrayList<BufferedImage>();
        vectors = new ArrayList<MatchVector>();
    }

    /**
     * Parametrized constructor.
     * Vectors can be null
     * @param images Frame sequence.
     * @param vectors Set of match vectors.
     */
    public Video(List<BufferedImage> images, List<MatchVector> vectors) {
        this.frames = images;
        this.vectors = vectors;
        if(this.vectors == null) {
            this.vectors = new ArrayList<MatchVector>();
        }
    }

    /**
     * Gets frame sequence.
     * @return Frame sequence.
     */
    public List<BufferedImage> getFrames() {
        return frames;
    }
    
    /**
     * Gets a concrete frame.
     * @param n Sequence frame index.
     * @return A concrete frame.
     */
    public BufferedImage getFrame(int n) {
        return frames.get(n);
    }

    /**
     * Sets a frame sequence.
     * @param frames Frame sequence.
     */
    public void setFrames(List<BufferedImage> frames) {
        this.frames = frames;
    }
    
    /**
     * Adds a frame to the sequence.
     * @param frame Frame to add.
     */
    public void addFrame(BufferedImage frame) {
        if(frames == null) {
            frames = new ArrayList<BufferedImage>();
        }
        frames.add(frame);
    }

    /**
     * Gets the list of match vectors.
     * @return List of match vectors.
     */
    public List<MatchVector> getVectors() {
        return vectors;
    }

    /**
     * Sets the list of match vectors.
     * @param vectors List of match vectors.
     */
    public void setVectors(List<MatchVector> vectors) {
        this.vectors = vectors;
    }
    
    /**
     * Adds a vector to the list.
     * @param vector The vector to add.
     */
    public void addVector(MatchVector vector) {
        if(vectors == null) {
            vectors = new ArrayList<MatchVector>();
        }
        vectors.add(vector);
    }

    /**
     * Gets tessel height.
     * @return Tessel height.
     */
    public int gettHeight() {
        return tHeight;
    }

    /**
     * Gets tessel width.
     * @return Tessel width.
     */
    public int gettWidth() {
        return tWidth;
    }
    
    /**
     * Gets base frame index.
     * @return Base frame index.
     */
    public int getBaseCount() {
        return baseCount;
    }
    
    /**
     * Gets the list of vectors for one frame.
     * @param nFrame Frame index.
     * @return List of vectors for one frame.
     */
    public List<MatchVector> getVectorsForFrame(int nFrame) {
        List<MatchVector> ret = new ArrayList<MatchVector>();
        for(MatchVector v : vectors) {
            if(v.nFrame == nFrame) {
                ret.add(v);
            } else if(v.nFrame > nFrame) {
                break;
            }
        }
        return ret;
    }
    
    /**
     * Stores the video in HSD format.
     * @param path Path to save video.
     * @param tHeight Tessel height.
     * @param tWidth Tessel width.
     * @param baseCount Base image index.
     * @throws IOException Thrown if there is a problem with the file system.
     */
    public void saveToFile(String path, int tHeight, int tWidth, int baseCount)
            throws IOException {
        FileOutputStream fout = new FileOutputStream(path);
        GZIPOutputStream out = new GZIPOutputStream(fout);
        ObjectOutputStream oos = new ObjectOutputStream(out);
        
        //CABECERA--------------------------------------------------------------
        //Esta cabecera es necesaria para la posterior lectura del fichero.
        oos.writeInt(tHeight);        //Alto de tesela.
        oos.writeInt(tWidth);         //Ancho de tesela.
        oos.writeInt(baseCount);      //Indice de marco base.
        
        oos.writeInt(frames.size());  //Numero de marcos guardados.
        oos.writeInt(vectors.size()); //Numero de vectores guardados.
        //FIN CABECERA----------------------------------------------------------
        
        //MARCOS----------------------------------------------------------------
        ByteArrayOutputStream byteArray;
        //Cada marco se codifica en JPEG y se obtienen los bytes.
        //Tales bytes son escritos COMO OBJETO JAVA para separarlos unos de otros.
        for(BufferedImage frame: frames) {
            byteArray = new ByteArrayOutputStream();
            ImageIO.write(frame, "JPEG", byteArray);
            oos.writeObject(byteArray.toByteArray());
            byteArray.flush();
            oos.flush();
            byteArray.close();
        }
        //FIN MARCOS------------------------------------------------------------
        
        //VECTORES--------------------------------------------------------------
        //Escritura de los vectores tambien como objetos java.
        for(MatchVector vector: vectors) {
            oos.writeObject(vector);
            oos.flush();
        }
        //FIN VECTORES----------------------------------------------------------
        
        oos.close();
        out.close();
        fout.close();
    }
    
    /**
     * Loads a video from a file.
     * @param path File path.
     * @throws FileNotFoundException Thrown if the file does not exist.
     * @throws IOException Thrown if there is a problem with the file system.
     * @throws ClassNotFoundException Thrown if the file is corrupted.
     */
    public void loadFromFile(String path)
            throws FileNotFoundException,
            IOException,
            ClassNotFoundException {
        FileInputStream fin = new FileInputStream(path);
        GZIPInputStream in = new GZIPInputStream(fin);
        ObjectInputStream ios = new ObjectInputStream(in);
        
        //CABECERA--------------------------------------------------------------
        tHeight = ios.readInt();
        tWidth = ios.readInt();
        baseCount = ios.readInt();
        
        int nFrames = ios.readInt();
        int nVectors = ios.readInt();
        //FIN CABECERA----------------------------------------------------------
        
        //MARCOS----------------------------------------------------------------
        byte[] bytes;
        BufferedImage img;
        ByteArrayInputStream byteArray;
        
        for(int i = 0; i < nFrames; i++) {
            bytes = (byte[]) ios.readObject();
            byteArray = new ByteArrayInputStream(bytes);
            img = ImageIO.read(byteArray);
            frames.add(img);
            byteArray.close();
        }
        //FIN MARCOS------------------------------------------------------------
        
        //VECTORES--------------------------------------------------------------
        MatchVector vec;
        for(int i = 0; i < nVectors; i++) {
            vec = (MatchVector) ios.readObject();
            vectors.add(vec);
        }
        //FIN VECTORES----------------------------------------------------------
        
        ios.close();
        in.close();
        fin.close();
    }
    
    /**
     * Creates and loads a new video from a file.
     * @param path File path.
     * @return Loaded video.
     * @throws FileNotFoundException Thrown if the file does not exist.
     * @throws IOException Thrown if there is a problem with the file system.
     * @throws ClassNotFoundException Thrown if the file is corrupted.
     */
    public static Video loadNewVideoFromFile(String path)
            throws FileNotFoundException,
            IOException,
            ClassNotFoundException {
        Video ret = new Video();
        ret.loadFromFile(path);
        return ret;
    }
    
}
