/*
 * ZipLoader.java is part of HDS video codec.
 *
 * HDS video codec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HDS video codec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HDS video codec.  If not, see <http://www.gnu.org/licenses/>.
 */

package ub.edu.tmm.huelamosolans.zip;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import tsm.practiques.pTSM;
import tsm.practiques.pTSMComparator;

/**
 *
 * @author albertohuelamosegura
 */
public class ZipLoader {

    public static final String TMP_PATH = "./temp.png";
    //public static final String TMP_PATH = ".\\temp.png";
    public static final int BUF_SIZE = 9000;
    
    private ArrayList<pTSM> list;
    
    public ZipLoader() {
        list = new ArrayList<pTSM>();
    }
    
    public ArrayList<pTSM> getImages() {
        return list;
    }
    
    public void setImages(List<pTSM> list) {
        this.list = (ArrayList<pTSM>) list;
    }
    
    public void load(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        ZipInputStream zis = new ZipInputStream(fis);
        ZipEntry entry = zis.getNextEntry();
        
        FileOutputStream fos;
        BufferedOutputStream dest;
        FileInputStream fin;
        
        pTSM p;
        int count;
        byte[] data = new byte[BUF_SIZE];
        
        BufferedImage bi;
        
        while(entry != null) {
            if(!entry.isDirectory()) {
                
                fos = new FileOutputStream(TMP_PATH);
                dest = new BufferedOutputStream(fos, BUF_SIZE);
                
                while ((count = zis.read(data, 0, BUF_SIZE)) != -1) {
                    dest.write(data, 0, count);
                    dest.flush();
                }
                
                dest.close();
                fos.close();
                
                fin = new FileInputStream(TMP_PATH);
                bi = ImageIO.read(fin);
                bi.flush();
                fin.close();
                p = new pTSM(bi, entry.getName());
                
                list.add(p);
            }
            entry = zis.getNextEntry();
        }
        zis.close(); fis.close();
        Collections.sort(list, new pTSMComparator());
    }
    
}
