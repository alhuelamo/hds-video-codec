HDS video codec
===============
Practical project for Multimedia Technologies subject of Software Engineering degree at University of Barcelona, fall semester 2012.

David Solans Noguero

Alberto Huélamo Segura

This program have a GUI that controls all the functionalities:

* Read ZIP image files and load on memory.
* Read and load an HDS file applying a movement detection/compensation algorithm.
* Play video sequence cyclically.
* Photo filter application to all sequence.

This software and all its files are under GNU GPL v3.0 license. See LICENSE.txt to read more about GNU GPL v3.0 license.